B:=.build
LR:=.latexrun
DPI:=300
RES:=res/
T=itsec.pdf

SHELL:=bash

SVGS:=$(wildcard $(RES)*.svg)
PDFS:=$(SVGS:.svg=.pdf)
PNGS:=$(SVGS:.svg=.png)

$T:$(wildcard *.tex) $(wildcard *.bib) $(wildcard *.bst) $(PDFS) makefile $(LR)
	@mkdir -p '$B'
	./$(LR) -W no-fancyhdr --color=always -O $B 'itsec.tex'

.PHONY:all
all:$T

test:
	echo "$(SVGS)"

$(PDFS): %.pdf: %.svg
	@echo 'Building $@'
	@inkscape --batch-process --export-dpi=$(DPI) --export-filename='$@' '$<' 2>&1 | grep -v '** (org.inkscape' | grep . ; test "$${PIPESTATUS[0]}" -eq 0

$(PNGS): %.png: %.svg
	@echo 'Building $@'
	@inkscape --batch-process --export-dpi=$(DPI) --export-filename='$@' '$<' 2>&1 | grep -v '** (org.inkscape' | grep . ; test "$${PIPESTATUS[0]}" -eq 0

.PHONY:clean
clean:
	mkdir -p '$B' && rm -r '$B' $(wildcard $T) $(wildcard $(PDFS)) $(wildcard $(PNGS))
$(LR):
	@echo "Downloading latexrun ..."
	wget --no-clobber --no-verbose 'https://raw.githubusercontent.com/aclements/latexrun/master/latexrun' -O '$(LR)'
	chmod u+x '$(LR)'

