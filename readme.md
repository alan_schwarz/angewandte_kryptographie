# Broschüre: Angewandte Kryptographie

Das pdf kann aus den Quellen vollständig neu gebaut werden mit: `make -j4`.
Benötigt werden eine vollständige LaTeX Installation und inkscape zum rendern der svgs.

Hinweis: Die Kompliation dauert mehrere Minuten.

## Contributions

Es werden Änderungsvorschläge als diffs oder pull requests gerne entgegen genommen. Anfragen zu passenden Grafiken sehr gerne per Mail.

## Author\*innen

Alan Schwarz
